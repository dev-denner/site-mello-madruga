

            <!-- Slider -->
            <div class="row slider" id="slides">
                
               
                
                <ul class="slides_container span12">
                    
                    
                    <?php
if ( function_exists( 'get_option_tree' ) ) {
  $slides = get_option_tree( 'slides', $option_tree, false, true, -1 );
  foreach( $slides as $slide ) { ?>
                    
                 

        <li class="slider-item active">
            

   <?php if($slide['image']) { ?>
            
            <a href="<?php echo $slide['link'] ?>">
                

                            <div class="span3 slider-imgs">
                            
                                <img src="<?php echo get_template_directory_uri(); ?>/includes/timthumb.php?q=100&amp;w=270&amp;h=270&amp;zc=1&amp;src=<?php echo $slide['image'] ?>" alt="<?php echo $slide['title'] ?>" class="slider-img" />
                                
                        </div>
                
                       
                            
                        </a>
            
         

 <div class="span8 slider-text">
                            
                            <h1><?php echo $slide['slidetitle'] ?></h1>
   
                                <?php echo do_shortcode($slide['slidetext']) ?>
                             
                        </div>
            
                        <?php } else { ?>
            
             <div class="span10 slider-text">
                            
                            <h1><?php echo $slide['slidetitle'] ?></h1>
   
                                <?php echo do_shortcode($slide['slidetext']) ?>
                             
                        </div>
            
                        <?php } ?>

    </li>

<?php

             

  }
}
?>
                    
                </ul>
                <!-- // End of Slider Nav -->
                    
            </div>
            <!-- // End Of Slider -->
            