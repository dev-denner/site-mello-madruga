<?php
/*
 * Plugin Name: DD Testimonials Widget
 * Plugin URI: http://themeforest.net/user/DDStudios/portfolio
 * Description: A widget that displays testimonials
 * Version: 1.0
 * Author: Dany Duchaine
 * Author URI: http://themeforest.net/user/DanyDuchaine/
 */

/*
 * Add function to widgets_init that'll load our widget.
 */
add_action( 'widgets_init', 'dd_testimonials_widget' );

/*
 * Register widget.
 */
function dd_testimonials_widget() {
	register_widget( 'dd_testimonials_widget' );
}

/*
 * Widget class.
 */
class dd_testimonials_widget extends WP_Widget {

	/* ---------------------------- */
	/* -------- Widget setup -------- */
	/* ---------------------------- */

	function dd_testimonials_widget() {

		/* Widget settings. */
		$widget_ops = array( 'classname' => 'dd_testimonials_widget', 'description' => __('A widget that displays testimonials.', 'localization') );

		/* Widget control settings. */
		$control_ops = array( 'width' => 300, 'height' => 350, 'id_base' => 'dd_testimonials_widget' );

		/* Create the widget. */
		$this->WP_Widget( 'dd_testimonials_widget', __('DD Testimonials Widget', 'localization'), $widget_ops, $control_ops );
	}

	/* ---------------------------- */
	/* ------- Display Widget -------- */
	/* ---------------------------- */
	
	function widget( $args, $instance ) {
		extract( $args );

		/* Our variables from the widget settings. */
		$title = apply_filters('widget_title', $instance['title'] );
	

		/* Before widget (defined by themes). */
		echo $before_widget;

		/* Display the widget title if one was input (before and after defined by themes). */
		if ( $title )
			echo $before_title . $title . $after_title;

		/* Display Widget */
		?>

               <!-- Testimonial Widgets -->
                <div id="testimonial-slides">
                    
                    <div class="slides_container">


                      <?php
        global $paged;

        $arguments = array(
            'post_type' => 'testimonials_posts',
            'post_status' => 'publish',
            'paged' => $paged
        );

        $testimonials_query = new WP_Query($arguments);

        dd_set_query($testimonials_query);
		$i = 0;
        ?>

        <?php if ($testimonials_query->have_posts()) : while ($testimonials_query->have_posts()) : $testimonials_query->the_post();


        	//STARTS OUR ITEM

        ?>


                        
                        <!-- Testimonial Item -->
                        <div class="testimonial-item active clearfix">
                            
                            <div class="testimonial-meta clearfix">
                                
                                
                                        <?php if(get_post_meta(get_the_id(), 'ddtestimonials', true) != '') : //IF IT HAS AT LEAST ONE THUMB

							//OUR THUMBNAILS
							$testimonials = ddListGet('testimonials', get_the_ID());?>
                                
                                <div class="testimonial-avatar">
                                    
                                     <img src="<?php get_template_directory_uri(); ?>/includes/timthumb.php?q=100&amp;w=50&amp;h=50&amp;zc=1&amp;src=<?php echo $testimonials[0]['testimonials_avatar']; ?>" alt="" />
  
                                </div>
                                
                                         <?php endif; ?>
                                    
                                <div class="testimonial-info">
                                    
                                    <span class="testimonial-name"><?php the_title(); ?></span>
                                    <a href="<?php echo $testimonials[0]['testimonials_url']; ?>" class="testimonial-website"><span><?php echo $testimonials[0]['testimonials_link']; ?></span></a>
                                        
                                </div>
                                    
                            </div>
                                
                            <div class="testimonial-content clearfix">
                                
                                <div class="testimonial-text">
                                    
                                   <?php the_content(); ?>
                                    
                                </div>
                                    
                            </div>
                                
                        </div>
                        <!-- // End Of Testimonial Item -->


                            <?php endwhile; ?>

        <?php endif; ?>   

                                            
                    </div>
                        
                </div>
                <!-- // End Of Testimonial Widgets -->

		<?php

		/* After widget (defined by themes). */
		echo $after_widget;
	}

	/* ---------------------------- */
	/* ------- Update Widget -------- */
	/* ---------------------------- */
	
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		/* Strip tags for title and name to remove HTML (important for text inputs). */
                $instance['title'] = strip_tags( $new_instance['title'] );
	
		

		/* No need to strip tags for.. */

		return $instance;
	}
	
	/* ---------------------------- */
	/* ------- Widget Settings ------- */
	/* ---------------------------- */
	
	/**
	 * Displays the widget settings controls on the widget panel.
	 * Make use of the get_field_id() and get_field_name() function
	 * when creating your form elements. This handles the confusing stuff.
	 */
	 
	function form( $instance ) {

		/* Set up some default widget settings. */
		$defaults = array(
                'title' => 'What Clients Says About Us'
				);
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>

	
       
		
		
		<!-- Postcount: Text Input -->
		
		
		<!-- Tweettext: Text Input -->
				
	<?php
	}
}
?>