<?php

/*

 * Plugin Name: DD Latest Tweets

 * Plugin URI: http://themeforest.net/user/DDStudios/portfolio

 * Description: A widget that displays tweets based on a query

 * Version: 1.0

 * Author: Dany Duchaine

 * Author URI: http://themeforest.net/user/DDStudios/

 */



/*

 * Add function to widgets_init that'll load our widget.

 */

add_action( 'widgets_init', 'dd_posts_widgets' );



/*

 * Register widget.

 */

function dd_posts_widgets() {

	register_widget( 'DD_Post_Widget' );

}



/*

 * Widget class.

 */

class dd_post_widget extends WP_Widget {



	/* ---------------------------- */

	/* -------- Widget setup -------- */

	/* ---------------------------- */



	function DD_Post_Widget() {



		/* Widget settings. */

		$widget_ops = array( 'classname' => 'post-widget', 'description' => __('A widget that displays your latest posts.', 'localization') );



                /* Widget control settings. */

		$control_ops = array( 'width' => 300, 'height' => 350, 'id_base' => 'dd_post_widget' );

		

		/* Create the widget. */

		$this->WP_Widget( 'dd_post_widget', __('DD Latest Posts','localization'), $widget_ops, $control_ops );

	}



	/* ---------------------------- */

	/* ------- Display Widget -------- */

	/* ---------------------------- */



	function widget( $args, $instance ) {

		extract( $args );



		/* Our variables from the widget settings. */

		$title = apply_filters('widget_title', $instance['title'] );

		$postcount = $instance['postcount'];

                $blogcategory = $instance['blogcategory'];



		/* Before widget (defined by themes). */

		echo $before_widget;



		/* Display the widget title if one was input (before and after defined by themes). */

		if ( $title )

			echo $before_title . $title . $after_title;



		/* Display Widget */



		/* Display Latest Posts */

		 ?>

<!-- blogwidget Widgets -->

<div id="blogwidget-slides">
  <div class="slides_container">
    <?php

        global $post;



        $category_id = get_cat_ID($blogcategory);



        $arguments = array(

            'post_type' => 'post',

            'post_status' => 'publish',

            'showposts' => $postcount,

            'paged' => $paged,

            'cat' => $category_id,

        );



        $blog_query = new WP_Query($arguments);



        dd_set_query($blog_query);

 

        ?>
    <?php if ($blog_query->have_posts()) : while ($blog_query->have_posts()) : $blog_query->the_post(); ?>
    
    <!-- blogwidget Item -->
    
    <div class="blogwidget-item active">
      <?php if (get_post_meta(get_the_id(), 'ddblogImg', true) != '') :



                    $blogImg = ddListGet('blogImg', get_the_ID()); ?>
      <div class="blogwidget-img"> <a href="<?php the_permalink(); ?>"><img src="<?php echo $blogImg[0]['img_url']; ?>" alt="" /></a> </div>
      <?php endif; ?>
      <div class="blogwidget-title"> <a href="<?php the_permalink(); ?>">
        <?php the_title(); ?>
        </a> </div>
      <div class="blogwidget-content clearfix">
        <div class="blogwidget-text">
          <div class="blogwidget-meta">
            <?php 

$arc_year = get_the_time('Y'); 

$arc_month = get_the_time('m'); 

$arc_day = get_the_time('d'); 

?>
            por
            <?php the_author_posts_link(); ?>
            em <a href="<?php echo get_day_link($arc_year, $arc_month, $arc_day); ?>">
            <?php the_time( get_option( 'date_format' ) ); ?>
            </a> </div>
          <?php the_excerpt(); ?>
        </div>
      </div>
    </div>
    
    <!-- // End of blogwidget Item -->
    
    <?php endwhile; ?>
    <?php dd_restore_query(); ?>
    <?php endif; ?>
  </div>
</div>

<!-- // End of blogwidget Widgets -->

<?php



                /* After widget (defined by themes). */

		echo $after_widget;

                

	}



	function update($new_instance, $old_instance) {

		return $new_instance;

	}



	function form($instance) {

		$title = esc_attr($instance['title']);

		$numposts = esc_attr($instance['numposts']);

		$blogcategory = esc_attr($instance['blogcategory']);



		?>
<p>
  <label for="<?php echo $this->get_field_id('blogcategory'); ?>">
    <?php _e('Blog Category:','localization'); ?>
  </label>
  <?php

            //Access the WordPress Categories via an Array

			$dd_categories = array();

			$dd_categories_obj = get_categories('hide_empty=0');

			foreach ($dd_categories_obj as $dd_cat) {

    			$dd_categories[$dd_cat->cat_ID] = $dd_cat->cat_name;}

			$categories_tmp = array_unshift($dd_categories, "Select a category:");

            ?>
  <select id="<?php echo $this->get_field_id('blogcategory'); ?>" name="<?php echo $this->get_field_name('blogcategory'); ?>">
    <?php

			//DISPLAY SELECT OPTIONS

			foreach ($dd_categories as $dd_category) {

				if ($blogcategory == $dd_category) {

					$selected_option = 'selected="selected"';

				} else {

					$selected_option = '';

				} ?>
    <option value="<?php echo $dd_category; ?>" <?php echo $selected_option; ?>><?php echo $dd_category; ?></option>
    <?php

			} ?>
  </select>
</p>
<p>
  <label for="<?php echo $this->get_field_id( 'postcount' ); ?>">
    <?php _e('Number of posts', 'localization') ?>
  </label>
  <input class="widefat" id="<?php echo $this->get_field_id( 'postcount' ); ?>" name="<?php echo $this->get_field_name( 'postcount' ); ?>" value="<?php echo $instance['postcount']; ?>" />
</p>
<?php

	}

	

}

?>
